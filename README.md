# Spring Boot CRUD Пример для Управления Продуктами

Данное приложение представляет собой пример системы для управления продуктами, разработанную на Spring Boot с использованием JPA.

## Установка и настройка

Для установки данного приложения необходимо склонировать проект из репозитория. Убедитесь, что у вас установлен Maven, и выполните сборку проекта с помощью команды:

```
mvn clean install
```

После успешной сборки вы можете запустить приложение

## Модель Продукта

Для создания объектов продуктов в приложении используется следующая модель:

```
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "PRODUCT_TBL")
public class Product {
@Id
@GeneratedValue
private int id;
private String name;
private int quantity;
private double price;
}
```

## Технологии

Проект разработан с использованием следующих технологий:
- Spring Boot Starter Data JPA
- Spring Boot Starter Web
- Spring Boot DevTools
- MariaDB Java Client
- Lombok

## Использование

Приложение предоставляет доступ к различным эндпоинтам для управления продуктами:
```
    @PostMapping("/addProduct")
    public Product addProduct(@RequestBody Product product){
    return productService.saveProduct(product);
    }

    @PostMapping("/addProducts")
    public List<Product> addProducts(@RequestBody List<Product> products){
        return productService.saveProducts(products);
    }
    
    @GetMapping("/products")
    public List<Product> findAllProducts(){
        return productService.getProducts();
    }
    
    @GetMapping("/productById/{id}")
    public Product findProductById(@PathVariable int id){
        return productService.getProductById(id);
    }
    
    @GetMapping("/product/{name}")
    public Product findProductByName(@PathVariable String name){
        return productService.getProductByName(name);
    }
    
    @PutMapping("/update")
    public Product updateProduct(@RequestBody Product product){
        return productService.updateProduct(product);
    }
    
    @DeleteMapping("/delete/{id}")
    public String deleteProduct(@PathVariable int id){
        return productService.deleteProduct(id);
    }
```

## Благодарности

Благодарю за интерес к  приложению!